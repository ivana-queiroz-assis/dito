# Desafio Backend - Dito feito por Ivana Assis

## Serviço de autocomplete e manipulação de dados 

* [Ferramentas utilizadas: Intellij, Maven, PostMan and Java](#softwares-tools)
* [Execução do projeto](#execução-do-projeto)
* [Cadastro de Eventos ](#cadastro-de-eventos)
* [Auto Complete ](#auto-complete)
* [Compras ](#compras)
* [Testes](#testes)
        
## Softwares Tools

- Java 8 
- Spring Tool Suite - 3.9.4.RELEASE
- PostMan
- Intellij 

## Execução do projeto

Executar a classe main 'DitoApplication' e acessar 'http://localhost:8080/dito/' 

Url onde a aplicação irá subir que contém a página do autocomplete sendo que o autocomplete já tem algumas palavras já criadas na árvore Trie.

O banco de dados foi feito utilizando o banco de dados em memória H2. Abaixo a URL para acesso:

    - H2: 
        - Endereço: http://localhost:8080/dito/h2-console
        - JDBC URL: jdbc:h2:mem:dito
 
 Para acessar as requisições Postman acessar a URL: 'https://documenter.getpostman.com/view/4150764/SVYqPJsm?version=latest'.   

 Para rodar os testes, utilizar o Maven.

## Cadastro de Eventos

  - **_Eventos_**:
  
    - **_Endpoint_**:
      - /evento, post: cria um novo evento passado no corpo da requisição.
    
    Ao se criar um novo evento ele irá aparecer na página html do autocomplete.
    
## Auto Complete     
 
  - **_Endpoint_**:
   
    - /suggestion/searchstr="", post: faz a busca na Trie de acordo com a string passada na variável "searchstr". 
    
    Foi feita uma carga inicial de eventos na árvore com um dicionário de palavras que foi carregada de um arquivo na pasta 'resources/dictionary'.
    
## Compras

  - **_Endpoint_**:
  
    - /compras, post: agrupa os eventos pela variável transaction_id em ordem desc pelo timestamp do evento.
   
 ## Testes
 
 - **_Eventos_**:
 
    O teste realizado em cima do cadastramento de eventos foi feita na camada controller por meio da classe 'EventoControllerTest'.
 
 - **_Manipulação de dados_**:   
 
    Já para o teste realizado na manipulação de dados foi feito na camada service na classe 'TimeLineServiceTest'.
    Onde foi testado se as compras foram ordenadas pelo timestamp da transação.
    
- **_Autocomplete_**:

    E o teste de perfomance para o autocomplete foi feita considerando a inserção de um conjunto de palavras lidas de um arquivo.
    
    Sendo que a o tempo de complexidade é **O(n)** e a complexidade de espaço na Trie é **O(nxm)**, onde m é o número de palavras na trie.

 
 
 Qualquer dúvida estou á disposição. Muito obrigada pelo teste.
 


