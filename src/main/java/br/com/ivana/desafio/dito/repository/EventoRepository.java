package br.com.ivana.desafio.dito.repository;

import br.com.ivana.desafio.dito.model.Evento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventoRepository extends JpaRepository<Evento,Long> {
}
