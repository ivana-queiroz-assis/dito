package br.com.ivana.desafio.dito.service;

import br.com.ivana.desafio.dito.model.TrieNode;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TrieService {

    private TrieNode root;

    public TrieService() {

        root = new TrieNode(' ');
        insert("buy");
        insert("sell");
        insert("cart");
        insert("sells");
        insert("selling");
        insert("carts");
        insert("item");
        insert("remove");
        insert("update");
        mockTrieAutoComplete();
    }

    private void mockTrieAutoComplete(){
        try {
            BufferedReader br = new BufferedReader(new
                    FileReader("src/main/resources/dictionary/portuguese-brazil_without-accents.dic.txt"));
            String linha;
            while ((linha = br.readLine()) != null) {
               insert(linha);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void insert(String word) {
        if (search(word))
            return;

        TrieNode current = root;
        TrieNode pre;
        for (char ch : word.toCharArray()) {
            pre = current;
            TrieNode child = current.getChild(ch);
            if (child != null) {
                current = child;
                child.setParent(pre);
            } else {
                current.addChildren(new TrieNode(ch));
                current = current.getChild(ch);
                current.setParent(pre);
            }
        }
        current.setEnd(true);
    }

    public boolean search(String word) {
        TrieNode current = root;
        for (char ch : word.toCharArray()) {
            if (current.getChild(ch) == null)
                return false;
            else {
                current = current.getChild(ch);
            }
        }
        if (current.isEnd()) {
            return true;
        }
        return false;
    }

    public List autocomplete(String prefix) {
        TrieNode lastNode = root;
        for (int i = 0; i < prefix.length(); i++) {
            lastNode = lastNode.getChild(prefix.charAt(i));
            if (lastNode == null)
                return new ArrayList();
        }
        return lastNode.getWords();
    }
}
