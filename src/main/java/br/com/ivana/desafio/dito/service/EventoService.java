package br.com.ivana.desafio.dito.service;

import br.com.ivana.desafio.dito.model.Evento;
import br.com.ivana.desafio.dito.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EventoService {

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private TrieService trieService;

    public Evento insert(Evento evento) {
        trieService.insert(evento.getEvent());
        return eventoRepository.save(evento);
    }

    public Optional<Evento> load(Long id) {
        return eventoRepository.findById(id);
    }

    public Evento update(Evento evento) {
        return eventoRepository.save(evento);
    }

    public void deleteById(Long id) {
        eventoRepository.deleteById(id);
    }

}
