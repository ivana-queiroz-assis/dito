package br.com.ivana.desafio.dito.service;

import br.com.ivana.desafio.dito.model.Compras;
import br.com.ivana.desafio.dito.model.Evento;
import br.com.ivana.desafio.dito.model.Product;
import br.com.ivana.desafio.dito.model.TimeLine;
import org.springframework.stereotype.Service;

import java.text.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
public class TimeLineService {

    public List<TimeLine> agrupaCompras(Compras compras) {
        List<TimeLine> listTimeLine = new ArrayList<>();
        for (Evento e : compras.getEvents()) {
            if (e.getEvent().equals("comprou")) listTimeLine.add(addInformacoesCompra(e));

        }
        for (Evento e : compras.getEvents()) {
            if (e.getEvent().equals("comprou-produto")) addProdutos(e, listTimeLine);
        }

        return ordernaDate(listTimeLine);
    }

    private List<TimeLine> ordernaDate(List<TimeLine> listTimeLine ){
        listTimeLine.sort((o1, o2) -> o2.getTimestamp().compareTo(o1.getTimestamp()));
        return listTimeLine;
    }

    private TimeLine addInformacoesCompra(Evento e) {
        TimeLine timeLine = new TimeLine();
        timeLine.setTimestamp(e.getTimestamp());
        timeLine.setRevenue(e.getRevenue());
        timeLine.setTransaction_id(Integer.parseInt(e.getCustom_data().get("transaction_id")));
        timeLine.setStore_name(e.getCustom_data().get("store_name"));
        return timeLine;

    }

    private void addProdutos(Evento e, List<TimeLine> listTimeLine) {
        if (e.getCustom_data().containsKey("transaction_id")) {
            TimeLine timeLine = listTimeLine.stream().filter(
                    t -> String.valueOf(t.getTransaction_id()).
                            equals(e.getCustom_data().get("transaction_id"))).findFirst().get();
            if (timeLine.getTransaction_id() == Integer.parseInt(e.getCustom_data().get("transaction_id"))) {
                timeLine.setProduct(new Product(e.getCustom_data().get("product_name"),
                        Integer.parseInt(e.getCustom_data().get("product_price"))));
            }
        }
    }
}



