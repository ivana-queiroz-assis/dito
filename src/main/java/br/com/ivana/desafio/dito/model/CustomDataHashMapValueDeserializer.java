package br.com.ivana.desafio.dito.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.HashMap;

public class CustomDataHashMapValueDeserializer extends JsonDeserializer {
    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        HashMap<String, String> ret = new HashMap<>();

        ObjectCodec codec = jsonParser.getCodec();
        TreeNode node = codec.readTree(jsonParser);

        if (node.isArray()) {
            for (JsonNode n : (ArrayNode) node) {
                JsonNode id = n.get("key");
                if (id != null) {
                    JsonNode name = n.get("value");
                    ret.put(id.asText(), name.asText());
                }
            }
        }
        return ret;
    }
}
