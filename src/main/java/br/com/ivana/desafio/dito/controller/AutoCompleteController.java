package br.com.ivana.desafio.dito.controller;

import br.com.ivana.desafio.dito.model.SuggestionWrapper;
import br.com.ivana.desafio.dito.service.TrieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AutoCompleteController extends AbstractController {

	@Autowired
	private TrieService trieService;

	@GetMapping("/")
	public String autocomplete(Model model) {
		model.addAttribute("title", "autocomplete countries example");
		return "autocomplete";
	}

	@RequestMapping(value = "/suggestion", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public SuggestionWrapper autocompleteSuggestions(@RequestParam("searchstr") String searchstr) {
		List sulb = trieService.autocomplete(searchstr);
		SuggestionWrapper sw = new SuggestionWrapper();
		sw.setSuggestions(sulb);
		return sw;
	}
}
