package br.com.ivana.desafio.dito.model;

import java.util.List;

public class Compras {
   private List<Evento> events;

    public List<Evento> getEvents() {
        return events;
    }
    public void setEvents(List<Evento> events) {
        this.events = events;
    }
}
