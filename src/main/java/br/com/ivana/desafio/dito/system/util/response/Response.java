package br.com.ivana.desafio.dito.system.util.response;

import java.util.ArrayList;
import java.util.List;

public class Response<T> {

    private T data;
    private int status;
    private List<T> listData = new ArrayList<T>();
    private List<String> errors = new ArrayList<String>();

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Response(int status) {
        super();
        this.status = status;
    }

    public List<String> getErrors() {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<T> getListData() {
        return listData;
    }

    public void setListData(List<T> list) {
        this.listData = list;
    }

    public boolean hasErros() {
        return this.errors.isEmpty() ? false : true;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
