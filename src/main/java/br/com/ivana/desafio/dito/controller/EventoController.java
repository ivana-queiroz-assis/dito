package br.com.ivana.desafio.dito.controller;

import br.com.ivana.desafio.dito.model.Evento;
import br.com.ivana.desafio.dito.service.EventoService;
import br.com.ivana.desafio.dito.system.util.exception.SystemStatus;
import br.com.ivana.desafio.dito.system.util.response.Response;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;


@RestController
@RequestMapping("/evento")
public class EventoController extends AbstractController<Evento>{

    private static final Logger log = LoggerFactory.getLogger(EventoController.class);

    @Autowired
    private EventoService eventoService;

    @PostMapping
    public ResponseEntity<Response<Evento>> insert(@RequestBody Evento evento, BindingResult result) {
        Response<Evento> response = new Response<>(SystemStatus.SUCCESS.getCode());
        response.setData(eventoService.insert(evento));
        if (result.hasErrors()) {
            log.error("Erro ao inserir usuário: ", result.getAllErrors());
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }
        checkErrors(response);
        return ResponseEntity.ok(response);
    }

}
