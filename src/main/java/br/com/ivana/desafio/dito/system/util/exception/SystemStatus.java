package br.com.ivana.desafio.dito.system.util.exception;

public enum SystemStatus {

    SUCCESS(1, "Success", "success.system"),
    BUSINESS_ERROR(3, "Business Error", "error.system.business"),
    SERVER_ERROR(6, "Server Error", "error.system.server"),
    DATA_NOT_FOUND(5, "Data Not Found Error", "error.system.data.not.found");

    private int code;

    private String description;

    private String message;

    private SystemStatus(int code, String description, String message) {
        this.code = code;
        this.description = description;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
