package br.com.ivana.desafio.dito.system.util.exception;

import java.util.ArrayList;
import java.util.List;

public class SystemException  extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private SystemStatus status ;

    private List<String> errorMessages;

    public SystemException(String message) {
        this(SystemStatus.SERVER_ERROR, message);
    }

    public SystemException(){
        super();
    }
    public SystemException(SystemStatus status, String message) {
        this(status, message, null);
    }

    public SystemException(String message, Throwable root) {
        this(SystemStatus.SERVER_ERROR, message, root);
    }

    public SystemException(SystemStatus status, String message, Throwable root) {
        super(message, root);
        this.status = status;
        this.errorMessages = new ArrayList<>();
        this.errorMessages.add(message);
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public SystemStatus getStatus() {
        return status;
    }

    public void setStatus(SystemStatus status) {
        this.status = status;
    }
}

