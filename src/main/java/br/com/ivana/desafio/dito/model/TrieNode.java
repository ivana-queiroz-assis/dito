package br.com.ivana.desafio.dito.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TrieNode {

    private char data;
    private LinkedList<TrieNode> children;
    private TrieNode parent;
    private boolean isEnd;

    public TrieNode(char c) {
        data = c;
        children = new LinkedList();
        isEnd = false;
    }

    public TrieNode getChild(char c) {
        if (children != null)
            for (TrieNode eachChild : children)
                if (eachChild.data == c)
                    return eachChild;
        return null;
    }

    public List<String> getWords() {
        List list = new ArrayList();
        if (isEnd) {
            list.add(toString());
        }

        if (children != null) {
            for (int i=0; i< children.size(); i++) {
                if (children.get(i) != null) {
                    list.addAll(children.get(i).getWords());
                }
            }
        }
        return list;
    }

    public String toString() {
        if (parent == null) {
            return "";
        } else {
            return parent.toString() + new String(new char[] {data});
        }
    }

    public void addChildren(TrieNode trieNode){
        this.children.add(trieNode);
    }

    public char getData() {
        return data;
    }

    public void setData(char data) {
        this.data = data;
    }

    public LinkedList<TrieNode> getChildren() {
        return children;
    }

    public void setChildren(LinkedList<TrieNode> children) {
        this.children = children;
    }

    public TrieNode getParent() {
        return parent;
    }

    public void setParent(TrieNode parent) {
        this.parent = parent;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }
}
