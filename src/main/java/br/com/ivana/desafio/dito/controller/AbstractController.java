package br.com.ivana.desafio.dito.controller;

import br.com.ivana.desafio.dito.system.util.exception.SystemStatus;
import br.com.ivana.desafio.dito.system.util.response.Response;

public abstract class AbstractController<T> {

    void checkErrors(Response<T> response) {
        if (response.hasErros()) {
            response.setStatus(SystemStatus.BUSINESS_ERROR.getCode());
        }
    }
}

