package br.com.ivana.desafio.dito.model;

import java.util.List;

public class SuggestionWrapper {
    List suggestions;

    public List getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List suggestions) {
        this.suggestions = suggestions;
    }
}
