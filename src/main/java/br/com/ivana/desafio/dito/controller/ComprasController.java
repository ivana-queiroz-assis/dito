package br.com.ivana.desafio.dito.controller;

import br.com.ivana.desafio.dito.model.Compras;
import br.com.ivana.desafio.dito.model.Evento;
import br.com.ivana.desafio.dito.model.TimeLine;
import br.com.ivana.desafio.dito.service.EventoService;
import br.com.ivana.desafio.dito.service.TimeLineService;
import br.com.ivana.desafio.dito.system.util.exception.SystemStatus;
import br.com.ivana.desafio.dito.system.util.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/compras")
public class ComprasController extends AbstractController {

    @Autowired
    private EventoService eventoService;

    @Autowired
    private TimeLineService timeLineService;

    @PostMapping
    public ResponseEntity<Response<TimeLine>> insert(@RequestBody Compras compras, BindingResult result) {
        Response<TimeLine> response = new Response<>(SystemStatus.SUCCESS.getCode());
        List<Evento> eventosSalvos = new ArrayList<>();
        compras.getEvents().forEach(e -> {
            eventoService.insert(e);
            eventosSalvos.add(e);
        });
        response.setListData(timeLineService.agrupaCompras(compras));
        if (result.hasErrors()) {
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }
        checkErrors(response);
        return ResponseEntity.ok(response);
    }
}
