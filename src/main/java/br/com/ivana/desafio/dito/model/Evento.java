package br.com.ivana.desafio.dito.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashMap;

@Entity
public class Evento implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String event;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX")
	private ZonedDateTime timestamp;

	private double revenue;

	public double getRevenue() {
		return revenue;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	@JsonDeserialize(using = CustomDataHashMapValueDeserializer.class)
	private HashMap<String, String> custom_data;

	public Evento(String event) {
		this.event = event;
	}

	public Evento(String event, ZonedDateTime timestamp, double revenue, HashMap<String, String> custom_data) {
		this.event = event;
		this.timestamp = timestamp;
		this.revenue = revenue;
		this.custom_data = custom_data;
	}

	public Evento(String event, ZonedDateTime timestamp, HashMap<String, String> custom_data) {
		this.event = event;
		this.timestamp = timestamp;
		this.custom_data = custom_data;
	}

	public Evento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(ZonedDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public void setCustom_data(HashMap<String, String> custom_data) {
		this.custom_data = custom_data;
	}

	public HashMap<String, String> getCustom_data() {
		return custom_data;
	}

	public Evento(String event, ZonedDateTime timestamp) {
		this.event = event;
		this.timestamp = timestamp;
	}
}

