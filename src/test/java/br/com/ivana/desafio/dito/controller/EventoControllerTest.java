package br.com.ivana.desafio.dito.controller;

import br.com.ivana.desafio.dito.model.Evento;
import br.com.ivana.desafio.dito.service.EventoService;
import br.com.ivana.desafio.dito.service.TrieService;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EventoControllerTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private EventoService eventoService;

	@MockBean
	private TrieService trieService;

	private JacksonTester<Evento> jsonEvento;

	private static final String apiEvento = "/evento";

	@Test
	public void canCreateNewEvent() throws Exception {
		BDDMockito.given(this.eventoService.insert(Mockito.any(Evento.class))).willReturn(new Evento("teste", ZonedDateTime.parse("2016-09-22T13:57:31.2311892-04:00")));

		JSONObject json = new JSONObject();
		json.put("nome", "teste");
		json.put("timestamp", "2016-09-22T13:57:31.2311892-04:00");

		MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.post(apiEvento)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json.toString()))
				.andReturn().getResponse();
		assertThat(response.getContentAsString()).contains("\"status\":1");
	}

}
