package br.com.ivana.desafio.dito.service;

import br.com.ivana.desafio.dito.model.Compras;
import br.com.ivana.desafio.dito.model.Evento;
import br.com.ivana.desafio.dito.model.TimeLine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TimeLineService.class})
public class TimeLineServiceTest {

	@Autowired
	private TimeLineService timeLineService;

	@Test
	public void whenAgrupaCompras_thenReturnEventosInOrderAndCheckProducts() {
		Compras compras = new Compras();
		List<Evento> listaEventos = new ArrayList<>();

		// Compras no shopping Norte
		HashMap<String, String> customDataComprouNorte = new HashMap<>();
		customDataComprouNorte.put("store_name", "Shopping Norte");
		customDataComprouNorte.put("transaction_id", "333333");
		Evento eventoComprouNorte = new Evento("comprou", ZonedDateTime.parse("2016-09-22T13:57:31.2311892-04:00"), 200.00, customDataComprouNorte);
		HashMap<String, String> customDataComprouProdutoNorte = new HashMap<>();
		customDataComprouProdutoNorte.put("product_name", "Calça Jeans Azul");
		customDataComprouProdutoNorte.put("transaction_id", "333333");
		customDataComprouProdutoNorte.put("product_price", "200");
		Evento eventoComprouProdutoNorte = new Evento("comprou-produto", ZonedDateTime.parse("2016-09-22T13:57:31.2311892-04:00"), customDataComprouProdutoNorte);
		listaEventos.add(eventoComprouNorte);
		listaEventos.add(eventoComprouProdutoNorte);

		//Compras no Shopping O Ponto
		HashMap<String, String> customDataComprouPonto = new HashMap<>();
		customDataComprouPonto.put("store_name", "Shopping Ponto");
		customDataComprouPonto.put("transaction_id", "22222");
		Evento eventoComprouPonto = new Evento("comprou", ZonedDateTime.parse("2018-09-22T13:57:31.2311892-04:00"), 100.00, customDataComprouPonto);
		HashMap<String, String> customDataComprouProdutoPonto = new HashMap<>();
		customDataComprouProdutoPonto.put("product_name", "Calça Jeans Azul");
		customDataComprouProdutoPonto.put("transaction_id", "22222");
		customDataComprouProdutoPonto.put("product_price", "100");
		Evento eventoComprouProdutoPonto = new Evento("comprou-produto", ZonedDateTime.parse("2018-09-22T13:57:31.2311892-04:00"), customDataComprouProdutoPonto);

		listaEventos.add(eventoComprouPonto);
		listaEventos.add(eventoComprouProdutoPonto);

		compras.setEvents(listaEventos);
		List<TimeLine> listaTimeLine = timeLineService.agrupaCompras(compras);

		assertThat(listaTimeLine.get(0).getTimestamp()).isSameAs(eventoComprouPonto.getTimestamp());
		assertThat(listaTimeLine.size()).isEqualTo(2);
		listaTimeLine.forEach(t->{
			assertThat(t.getProducts().size()).isEqualTo(1);
		});
	}




}
